<?php

add_action( 'widgets_init', function(){
	register_widget('Latest_Posts_By_Category');
});


class Latest_Posts_By_Category extends WP_Widget {

    function __construct() {
    	$this->defaults = [
    		// 'category' => get_option('default_category'),
    		'count' => '3',
    		'widget-title' => 'Latest Posts'
    	];
    	$this->fields = [
    		'count' => 'How many posts to display',
    		// 'category' => 'Which category to use',
    		'widget-title' => 'Title for the widget section'
    	];
        // $widget_ops = array( 'classname' => 'CSS class name', 'description' => 'Description' );
        parent::__construct( 'latests-posts-widget', 'Latest Posts by Category');
    }

    function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		echo $before_widget;
		echo $before_title;
		echo $instance['widget-title'];
		echo $after_title;

        // DO STUFF
        $cat = $instance['category'];
        $count = $instance['count'];
        $latest = new WP_Query([
            'posts_per_page' => $count,
            'category__in' => [$cat]
        ]);
        if ( $latest->have_posts() ) : while ( $latest->have_posts() ) : $latest->the_post(); ?>
            <a class="wpcontent-area post-in-list" href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail(array(100,100),array('class'=>'thumb')); ?>
            <h2><?php the_title() ?></h2>
            <p class="post-meta"><?php the_time('F j, Y') ?></p>
            <?php if( get_the_excerpt() )
                $c = get_the_excerpt() . '&hellip;';
            else
                list($c) = str_split(get_the_content(),30) . '&hellip;';

            if( is_search() ) {
                $c = str_ireplace(get_search_query(),'<span class="search-hilite-match">'.get_search_query().'</span>',$c);
            }
            
            echo '<div class="post-excerpt">'.$c.'</div>';
            ?>
            </a><!--.wpcontent-area-->
        <?php
        endwhile;endif;
        echo '<p class="read-more-link"><a href="'.get_category_link($cat).'">read more</a></p>';
        wp_reset_postdata();

    	echo $after_widget;
    }

    function update( $new_instance, $old_instance ) {
        $updated_instance = $new_instance;
        return $updated_instance;
    }

    function form( $instance ) {
		foreach( $this->defaults as $k=>$default) {
			$instance[$k] = ($instance[$k] == null) ? $default : $instance[$k];
		}

        $defaultCategory = $instance['category'] == null
            ? get_option('default_category')
            : $instance['category'];

        echo '<label for="'.$this->get_field_id('category').'">Which category to display posts from?</label>';
        wp_dropdown_categories([
            'name' => $this->get_field_name('category'),
            'id' => $this->get_field_id('category'),
            'selected' => $instance['category']
        ]);

		foreach( $this->fields as $k => $v ) {
			$placeholder = $this->defaults[$k];
			echo '<p>
				<label for="'.$this->get_field_id($k) . '">'.$v.'</label><br>
				<input type="text" id="' . $this->get_field_id($k) . '" name="'.$this->get_field_name($k).'" value="' . $instance[$k] . '" placeholder="'.$placeholder.'" >
				</p>
			';
		}
    }
}
