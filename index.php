<?php
get_header();
?>
	<div class="content column">
	
	<?php if ( have_posts() ) {

		if(is_author()) {
			$title = 'Articles by ' . get_the_author();
		} else if (is_category()) {
			$title = single_cat_title('',false);
		} else if (is_tag()) {
			$title = '#'.single_tag_title('',false);;
		} else if (is_search()) {
			$title = 'Search Results for "' . get_search_query() . '"';
		} else if (is_tax() ){

			$term = get_term_by('slug',get_query_var('term'),get_query_var('taxonomy'));

			if( $term ) :
				if( is_tax('series') ) {
					$title = "Sermons in the {$term->name} series";
				} else if (is_tax('speaker')) {
					$title = "Sermons by {$term->name}";
				}
			endif;

		} 
		?>
		
		<h1 class="page-title"><?php echo $title; ?></h1>


		<?php
		if( get_post_type() === 'sermon' ) {
			echo '<div class="sermons-list">';
		}

		while ( have_posts() ) : 
			the_post();

			if( get_post_type() === 'sermon' ) {
				get_template_part('snippet', 'sermon-in-list');
			} else {
				get_template_part('snippet','post-in-list');
			}

		endwhile; 

		if( get_post_type() === 'sermon' ) {
			echo '</div>';
		}
	
	} else { 
	?>

	  <div class="wpcontent-area">
	  <h1>Sorry</h1>
	  <p>Nothing to display here currently.</p>
	  </div>

	<?php 
	} 
	?>

	</div><!--.content.column-->

<?php get_footer();
