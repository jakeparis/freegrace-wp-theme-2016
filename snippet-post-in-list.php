
<a class="wpcontent-area post-in-list" href="<?php the_permalink(); ?>">
	<h2><?php the_title() ?></h2>
	<p class="post-meta"><?php the_time('F j, Y') ?></p>
	<?php 
	if( get_the_excerpt() )
		$c = get_the_excerpt();
	else {
		list($c) = str_split(get_the_content(),200);
		$c .= '&hellip;';
	}

	if( is_search() ) {
		$c = str_ireplace(get_search_query(),'<span class="search-hilite-match">'.get_search_query().'</span>',$c);
	}
	
	the_post_thumbnail(array(100,100),array('class'=>'thumb'));
	echo '<div class="post-excerpt">'.$c.'</div>';
	?>



</a><!--.wpcontent-area-->
