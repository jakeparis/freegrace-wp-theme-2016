<?php
/* 
Template Name: No Sidebar
*/
add_filter('body_class', function($classes){
	$classes[] = 'no-sidebar';
	return $classes;
});

get_header();
?>
	<div class="content column">
	  
	  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h1 class="page-title"><?php the_title() ?></h1>

		<div class="wpcontent-area">
			
			<?php the_content() ?>

		</div><!--.wpcontent-area-->

	  <?php endwhile; endif; ?>

	</div><!--.content.column-->

<?php get_footer(); ?>