<?php
defined('ABSPATH') || exit;
?>
<style>
	label.block {
		display: block;
		font-weight: bold;
		margin: 1em 0 .2em;
	}
	h2 {
		margin-top:  2em;
	}
	.wrap {
		font-size:  16px;
	}
	textarea.homepage-quote {
		width:  25em;
		height:  10em;

		text-align: center;
		font-family: 'IM Fell Great Primer';
		color: hsl(0,0%,100%);
		text-align: center;
		text-shadow: 0 0 2px hsl(0,0%,0%);
		font-size: 2em;
		font-weight: bold;
		padding:  1em;

		background: linear-gradient( 0deg, hsl(200, 36%, 28%), hsl(200, 36%, 36%));
	}
</style>
<div class="wrap">

	<h1>Free Grace: Website Settings</h1>

	<div class="notice">
		<p>Looking to customize the "important" message (displays at very top of homepage and other pages)? It lives under <a href="<?= admin_url('tools.php?page=important-message') ?>">Admin > Tools > Important Message</a>.</p>
	</div>
	
	<?php
	if( isset($_POST['submit']) ) {
		if( ! wp_verify_nonce( $_POST['_fg-settings-nonce'], 'save-fg-settings' ) )
				echo '<div class="notice notice-error"><p>There was a problem. Perhaps the form timed out?</p></div>';
			else {
				update_option('fgpca-service-time', $_POST['fgpca-service-time']);
				update_option('fgpca-email', $_POST['fgpca-email']);
				update_option('fgpca-phone', $_POST['fgpca-phone']);
				update_option('fgpca-address', $_POST['fgpca-address']);
				update_option('fgpca-homepage-quote', $_POST['fgpca-homepage-quote']);
		
				echo '<div class="notice notice-success"><p>Saved settings.</p></div>';
			}
	}

	$time = get_option('fgpca-service-time','');
	$email = get_option('fgpca-email', get_option('admin_email'));
	$phone = get_option('fgpca-phone', '');
	$address = get_option('fgpca-address', '');
	$homepageQuote = get_option('fgpca-homepage-quote', '');

	?>

	<form method="post" action="">

		<h2>Customization</h2>

		<label for="fgpca-service-time" class="block">Service Time</label>
		<input id="fgpca-service-time" type="text" name="fgpca-service-time" value="<?= esc_attr($time) ?>">

		<label for="fgpca-homepage-quote" class="block">Homepage Quote (preview)</label>
		<textarea name="fgpca-homepage-quote" id="fgpca-homepage-quote" class="homepage-quote"><?= htmlentities($homepageQuote) ?></textarea>

		<h2>Contact Information</h2>

		<label for="fgpca-email" class="block">Main Email Address (for theme)</label>
		<input type="email" id="fgpca-email" name="fgpca-email" value="<?= esc_attr($email) ?>">
		

		<label for="fgpca-phone" class="block">Phone Number</label>
		<input id="fgpca-phone" type="text" name="fgpca-phone" value="<?= esc_attr($phone) ?>">
		

		<label for="fgpca-address" class="block">Address</label>
		<input id="fgpca-address" name="fgpca-address" value="<?= esc_attr($address) ?>" type="text">

		<?php 
		submit_button();
		wp_nonce_field('save-fg-settings', '_fg-settings-nonce'); 
		?>

	</form>

</div>