<?php
defined('ABSPATH') || die('Not allowed');


add_shortcode( 'posts', function($atts,$content){
	$theseposts = new WP_Query($atts);
	if ( $theseposts->have_posts() ) : 
		ob_start();
		while ( $theseposts->have_posts() ) : $theseposts->the_post();
			get_template_part( 'snippet', 'post-in-list' );
		endwhile; 
		$out = ob_get_clean();
		return $out;
	else: 
		return '<p class="no-posts-found">Nothing to display here currently.</p>';
	endif;
});


add_shortcode('button', function($atts,$content){

	$atts = shortcode_atts( [
		'url' => '#',
		'class' => '',
		'style' => '',
	], $atts );

	$out = '';

	$url = esc_attr( $atts['url'] );
	$class = esc_attr( $atts['class'] );
	$class .= ' cta-button';
	$style = esc_attr($atts['style']);

	$out = "<a class='cta-button {$class}' href='{$url}' style='{$style}'>{$content}</a>";
	return $out;
});


add_shortcode('service-time', function($atts){
	// $atts = shortcode_atts( [
	// ], $atts );

	$settings = getAllFgpcaSettings();
	if( ! $settings['serviceTime'] )
		return '?';
	return $settings['serviceTime']; 
});