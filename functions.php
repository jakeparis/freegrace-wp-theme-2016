<?php
// ini_set('display_errors','1');
define('FREEGRACE_THEME_VERSION', '5.2.2');

require plugin_dir_path(__FILE__) . "/widgets.php";

add_action('after_setup_theme', function() {
	add_theme_support('menus');
	add_theme_support('post-thumbnails');
	
	add_theme_support( 'editor-styles' );

	add_theme_support('custom-header',[
		'width' => 1400,
		'height' => 350
	]);
	add_theme_support('title-tag');
	add_theme_support('widgets');
	add_theme_support('responsive-embeds');
	// add_theme_support('align-full');
	// add_theme_support('align-wide');

	add_image_size('really-large',1400,900);
});

add_action('wp_enqueue_scripts',function(){

	wp_register_style( 'googlefonts', '//fonts.googleapis.com/css?family=IM+Fell+Great+Primer|Poppins:300,400,700', [], '1' );

	wp_enqueue_style('fgpca-2017',get_stylesheet_uri(),['googlefonts'], FREEGRACE_THEME_VERSION);
	wp_enqueue_script('fgpca-2017',get_stylesheet_directory_uri().'/scripts.js',array('jquery'), FREEGRACE_THEME_VERSION);
	// wp_enqueue_script('mailchimp',get_stylesheet_directory_uri().'/mailchimp.js',array('jquery'),'1');
});

add_action('init', function(){
	// add_editor_style( );
	add_editor_style( [
		'https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer|Poppins:300,400,700',
		'style.css'
	] );
});


add_action('widgets_init',function(){
	$args = array(
	    array(
			'name'          => 'Main',
			'id'            => 'fg-sidebar-main',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h1 class="widgettitle">',
			'after_title'   => '</h1>'
		),
		array(
			'name'			=> 'Home Page',
			'id'			=> 'fg-sidebar-home',
			// 'before_widget'	=> '',
			// 'after_widget'	=> '',
			'before_title' 	=> '<h1 class="widgettitle">',
			'after_title'	=> '</h1>'
		),
		array(
			'name'			=> 'Contact Us',
			'id'			=> 'fg-sidebar-contact',
			'before_widget'	=> '',
			'after_widget'	=> '',
			'before_title' 	=> '<h1 class="widgettitle">',
			'after_title'	=> '</h1>'
		)
	);

	foreach($args as $argset)
		register_sidebar( $argset );

	add_filter('widget_text', 'do_shortcode');	
});

require_once 'shortcodes.php';

// deprecated section for FG settings
add_action('admin_init',function(){
	add_settings_section( 'fgpca-settings', 'Free Grace Specific Settings', 'fgpca_settings_section_callback', 'general' );
	function fgpca_settings_section_callback(){
		?>
		<p>Looking for Free Grace settings? They've moved to <a href="<?= admin_url('options-general.php?page=free-grace-options') ?>">thier own page</a>.</p>
		<?php
	}
});

function getAllFgpcaSettings(){

	static $out = array();
	if( ! empty($out) )
		return $out;

	$out = array(
		'serviceTime' => get_option('fgpca-service-time',''),
		'email' => get_option('fgpca-email', get_option('admin_email')),
		'phone' => get_option('fgpca-phone', ''),
		'address' => get_option('fgpca-address', '')
	);
	$out['emailLink'] = "mailto:" . $out['email'];
	if( $out['address'] )
		$out['addressLink'] = 'https://www.google.com/maps?hl=en&q=' . urlencode($out['address']);
	if( $out['phone'] )
		$out['phoneLink'] = 'tel:' . preg_replace('/[^0-9]*/','',$out['phone']);

	return $out;
}

function setImportantMessage($text,$style='snow'){
	if( ! $text ) {
		delete_option('fgpca-important-message');
		return;
	}

	$opt = array(
		'text' => stripslashes($text),
		'style' => $style
	);
	update_option('fgpca-important-message',$opt);
}
function getImportantMessage(){
	static $m = null;
	if( is_null($m) ) {
		$m = get_option('fgpca-important-message');
	}
	return $m;
}
function getImportantMessageText(){
	$opt = getImportantMessage();
	if( $opt )
		return $opt['text'];
	return false;
}
function getImportantMessageStyle($default=false){
	$opt = getImportantMessage();
	if( $opt )
		return $opt['style'];
	if( $default )
		return $default;
	return false;
}

add_action('admin_menu',function(){
	add_submenu_page( 'tools.php', 'Important Message', 'Important Message', 'edit_posts', 'important-message', 'importantMessageAdminPage' );
	add_options_page( 'Free Grace', 'Free Grace', 'edit_posts', 'free-grace-options', 'freeGrace_settingsPage', 1 );
});
function importantMessageAdminPage(){
	require 'importantMessage_adminPage.php';
}

function freeGrace_settingsPage () {
	require 'admin-themesettings.php';
}


/**
 * Updater
 */
require get_stylesheet_directory() . '/updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/freegrace-wp-theme-2016',
	__FILE__,
	'freegrace_2017'
);