<?php
if( ! class_exists('SermonsManager') ) {
	echo '<!-- The sermon plugin is not active -->';
	error_log( 'Sermon plugin not active!');
	return;
}

echo SermonsManager::printSingleSermonShort(get_post(),array('date'=>true));