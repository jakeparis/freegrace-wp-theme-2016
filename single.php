<?php
get_header();
?>
	<div class="content column">
	  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


		<h1 class="page-title"><?php the_title() ?></h1>

		<?php the_post_thumbnail('large' ); ?>

		<div class="wpcontent-area">

			<p class="post-meta">Written <?php the_date() ?> by <?php the_author_posts_link() ?></p>
			
			<?php the_content() ?>

		</div><!--.wpcontent-area-->

		<div class="meta-sub">
			<p>More by <?php the_author_posts_link() ?></p>
			<p>Categories: <?php the_category(', '); ?></p>
			<?php the_tags('<p>Tagged: ',', ', '</p>' ); ?>
		</div>

	  <?php endwhile; endif; ?>

	</div><!--.content.column-->

<?php get_footer(); ?>
