<?php
get_header();
?>
	<div class="content column">
	  
		<h1 class="page-title"><?php the_title() ?></h1>

		<div class="wpcontent-area">
			
			<p>Sorry, no page here like that. Please <a href="javascript:history.back()">try again</a> or search.</p>

			<?php get_search_form() ?>

		</div><!--.wpcontent-area-->

	</div><!--.content.column-->

<?php get_footer(); ?>