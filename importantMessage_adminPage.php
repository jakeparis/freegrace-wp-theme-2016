<?php
if( ! defined('ABSPATH') ) exit();
?>
<style>
	.header-message {
	  background-color: #d4d4d4;
	  border-radius: 3px;
	  border: 2px solid #1a1a1a;
	  color: black;
	  font-size: 1.1em;
	  margin: 1em 0 2em;
	  /*opacity: 0.9;*/
	  padding: 1.2em 2em;
	  text-align: center; 
	  transition:  all .3s;
	}

	.header-message.snow {
	  background-color: #aae8fd;
	  border-color: #269dd9; }

	.header-message.gold {
	  background-color: #fafdaa;
	  border-color: #ffd980; }

	.header-message.fire {
	  background-color: #bb4d3e;
	  color: white;
	  border-color: #bd1f0a; }

	.preview {
		background: hsl(10,57%,23%);
		padding: 1em;
	}
	.preview h2 {
		color:  hsl(0,0%,80%);
	}

	@media(min-width:550px){
		.img-msg-wrap {
			display:  flex;
		}
		.img-msg-wrap form,
		.preview 
		{
			box-sizing: border-box;
			width: 50%;
		}
	}
</style>

<div class="wrap">
	<?php
	if( isset( $_POST['submit'] ) ) {
		if( ! wp_verify_nonce( $_POST['_imp-msg-nonce'], 'save-important-message') ) {
			echo '<div class="notice notice-error"><p>There was an error saving. Perhaps the form timed out?</p></div>';

		} else {
			$text = $_POST['msg-text'];
			$style = $_POST['msg-style'];
			setImportantMessage($text,$style);
		}
	}

	$style = getImportantMessageStyle();
	$iMessage = getImportantMessageText();
	?>
	<h1>Important Message</h1>
	<p>Use this page to set a message at the top of the public site.</p>

	<div  class="img-msg-wrap">
	<form method="post" action="">
		<?php wp_nonce_field( 'save-important-message', '_imp-msg-nonce' ) ?>
		<h2>Message Color</h2>
		
		<input type="radio" name="msg-style" value="white" id="msg-style-white" <?= checked( $style, 'white',0) ?>> 
		<label for="msg-style-snow">Boring</label>
		<br>

		<input type="radio" name="msg-style" value="snow" id="msg-style-snow" <?= checked( $style, 'snow',0) ?>> 
		<label for="msg-style-snow">Snow</label>
		<br>

		<input type="radio" name="msg-style" value="gold" id="msg-style-gold" <?= checked( $style, 'gold',0) ?>> 
		<label for="msg-style-gold">Sunshine</label>
		<br>

		<input type="radio" name="msg-style" value="fire" id="msg-style-fire" <?= checked( $style, 'fire',0) ?>> 
		<label for="msg-style-fire">Fire</label>
		<br>

		<hr>

		<h2>Message to display</h2>
		<br>
		<?php wp_editor( $iMessage, 'msg-text', array(
			'media_buttons' => false,
			'textarea_rows' => 6,
			'teeny' => true,
			'quicktags' => false,
		) ); ?>
		
		<?php
		submit_button('Save');
		?>

	</form>

	<div class="preview" style="margin-left:1.5em;">
		<h2>Preview Your Message</h2>
		<?php
		if( ! $iMessage ) {
			$opacity = 'opacity: 0;';
		}
		echo '<div class="header-message '.esc_attr( $style ).'" style="'.$opacity.'">'.$text.'</div>';
	
		?>
		<script>
		window.addEventListener("load", function() {
			
			var styleSelect = document.querySelectorAll('[name="msg-style"]');
			var previewEl = document.querySelector('.header-message');
			for(var i=0;i<styleSelect.length;i++){
				styleSelect[i].addEventListener('change',function(e){
					previewEl.classList.remove('snow','fire','white','gold');
					previewEl.classList.add(this.value);
				});
			}

			const editor = tinymce.activeEditor;
			if( ! editor )
				return;

			var respondToEditor = function(e) {
				var content = editor.getContent()
				if(content == '' )
					previewEl.style.opacity = '0';
				else
					previewEl.style.opacity = '.9';

				previewEl.innerHTML = content;
			};

			editor.on('input', respondToEditor );
			editor.on('ExecCommand', respondToEditor );
			editor.on('mouseup', respondToEditor );
		});
		</script>

	</div>
</div>
