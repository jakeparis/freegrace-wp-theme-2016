<!DOCTYPE html>
<head>

<meta charset="UTF-8">
<meta name="author" content="Free Grace Presbyterian Church" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="description" content="<?= esc_attr( get_bloginfo('description') ) ?>" />
<meta name="keywords" content="" />

<?php 
if( is_singular() ) {
	$sidebarOption = get_post_meta( get_the_ID(), 'sidebar', true );
	if( in_array($sidebarOption, ['off','false','0','no']) ) {
		define('SIDEBAR_OFF', true);
	}
}
wp_head(); ?>

</head>

<?php 
	$bodyClass = [];
	$headerImg = (get_post_type() == 'page')
		? get_the_post_thumbnail_url(null,'really-large')
		: false;
	
	if( ! $headerImg )
		$headerImg = get_header_image();

	if( $headerImg )
		$headerImg = "background-image:url($headerImg);";

	if( defined('SIDEBAR_OFF') )
		$bodyClass[] = 'no-sidebar';

?>
<body <?php body_class($bodyClass,$post->ID) ?>>
	<svg style="display: none;" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<defs>
			<symbol id="icon-search" viewBox="0 0 951 1024" fill="#fff">
				<title>search</title>
				<path fill="#fff" class="path1" d="M658.286 475.429q0-105.714-75.143-180.857t-180.857-75.143-180.857 75.143-75.143 180.857 75.143 180.857 180.857 75.143 180.857-75.143 75.143-180.857zM950.857 950.857q0 29.714-21.714 51.429t-51.429 21.714q-30.857 0-51.429-21.714l-196-195.429q-102.286 70.857-228 70.857-81.714 0-156.286-31.714t-128.571-85.714-85.714-128.571-31.714-156.286 31.714-156.286 85.714-128.571 128.571-85.714 156.286-31.714 156.286 31.714 128.571 85.714 85.714 128.571 31.714 156.286q0 125.714-70.857 228l196 196q21.143 21.143 21.143 51.429z"></path>
			</symbol>
			<symbol id="icon-facebook" viewBox="0 0 1024 1024">
				<title>facebook</title>
				<path class="path1" d="M829.143 73.143q20 0 34.286 14.286t14.286 34.286v780.571q0 20-14.286 34.286t-34.286 14.286h-223.429v-340h113.714l17.143-132.571h-130.857v-84.571q0-32 13.429-48t52.286-16l69.714-0.571v-118.286q-36-5.143-101.714-5.143-77.714 0-124.286 45.714t-46.571 129.143v97.714h-114.286v132.571h114.286v340h-420q-20 0-34.286-14.286t-14.286-34.286v-780.571q0-20 14.286-34.286t34.286-14.286h780.571z"></path>
			</symbol>
		</defs>
	</svg>
  <header>
  	<span id="header-overlay" style="<?= $headerImg ?>"></span>
  	
  	<ul class="contact-info">

		<?php
			$fgpcaSettings = getAllFgpcaSettings();
		?>

  		<div class="wrapper">
	  		<li>Sunday Service: <?= $fgpcaSettings['serviceTime'] ?></li>
	  		<li><a href="<?= $fgpcaSettings['addressLink'] ?>"><?= $fgpcaSettings['address'] ?></a></li>
	  		<li><a href="<?= $fgpcaSettings['phoneLink'] ?>"><?= $fgpcaSettings['phone'] ?></a></li>
	  		<li><a href="<?= $fgpcaSettings['emailLink'] ?>"><?= $fgpcaSettings['email'] ?></a></li>
	  	</div>
  	</ul>

  	<div class="wrapper">

		<?php
		$message = getImportantMessageText();
		if( $message ){
			$style = getImportantMessageStyle();
			echo '<div class="header-message '.esc_attr( $style ).'">'.$message.'</div>';
		}
		?>

	  	<a href="<?php echo home_url() ?>" id="main-logo"></a>
		
		<div id="site-subhead">
			<p>
				<?= nl2br(get_option('fgpca-homepage-quote')) ?>
			</p>
	  	</div>
		


	  <nav class="main-nav">
	  	<a href="#" class="nav-more">Menu</a>
	  	<ul class="main-nav-ul">
	  	<li class="hide-550-up"><a class="js-close-main-nav main-nav-close">X close</a></li>
	  	<?php
	  	wp_nav_menu( array(
	  	            'menu'=>'main-navigation',
	  	            'container'=>'',
	  	            'items_wrap'=>'%3$s'
	  	            )
	  	);
	  	?>
		</ul>
	  </nav>

	</div>
  </header>


<div class="wrapper">
