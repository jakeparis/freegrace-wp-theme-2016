
<?php 
if( ! defined('SIDEBAR_OFF') )
	get_sidebar( ); 
?>

</div><!--.wrapper-->

<footer>
	<div class="wrapper">
		<?php get_search_form() ?>

		<a href="http://www.facebook.com/pages/Free-Grace-Presbyterian-Church/62730962143">
			<svg class="icon-facebook"><use xlink:href="#icon-facebook"></use></svg>
		</a>

		<h2>broken people, <br class="hide-550-up">bound together in Christ, <br class="hide-550-up">for His glory</h2>

		<p>Free Grace Presbyterian Church</p>
		<p>A member congregation of the <a href="https://www.pcanet.org/about-pca/">Presbyterian Church in America</a></p>
		<p>&nbsp;</p>

		<?php
		$fgpcaSettings = getAllFgpcaSettings();
		?>

		<p>
			<?php
			if( isset($fgpcaSettings['addressLink']) ) :
				echo '<a href="'. $fgpcaSettings['addressLink'] .'">
					' . $fgpcaSettings['address'] .'
				</a>';
			endif;
			if( isset($fgpcaSettings['phoneLink']) ) :
				echo '&nbsp;&nbsp; 
				<a href="' . $fgpcaSettings['phoneLink'] .'">
					' . $fgpcaSettings['phone'] . '
				</a>';
			endif;
			if( isset($fgpcaSettings['emailLink']) ) :
				echo ' &nbsp;&nbsp; 
				<a href="'. $fgpcaSettings['emailLink'] .'">
					' . $fgpcaSettings['email'] . '
				</a>';
			endif;
			?>
		</p>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
