/* SVG For Everybody @https://github.com/jonathantneal/svg4everybody */
(function(f,k,l,e,m){function g(a,c){if(c){var b=c.getAttribute("viewBox"),d=f.createDocumentFragment(),e=c.cloneNode(!0);for(b&&a.setAttribute("viewBox",b);e.childNodes.length;)d.appendChild(e.childNodes[0]);a.appendChild(d)}}function n(){var a=f.createElement("x"),c=this.s;a.innerHTML=this.responseText;this.onload=function(){c.splice(0).map(function(b){g(b[0],a.querySelector("#"+b[1].replace(/(\W)/g,"\\$1")))})};this.onload()}function h(){for(var a;a=k[0];){var c=a.parentNode,b=a.getAttribute("xlink:href").split("#"),
d=b[0],b=b[1];c.removeChild(a);if(d.length){if(a=e[d]=e[d]||new XMLHttpRequest,a.s||(a.s=[],a.open("GET",d),a.onload=n,a.send()),a.s.push([c,b]),4===a.readyState)a.onload()}else g(c,f.getElementById(b))}l(h)}m&&h()})(document,document.getElementsByTagName("use"),window.requestAnimationFrame||window.setTimeout,{},/Trident\/[567]\b/.test(navigator.userAgent)||537>(navigator.userAgent.match(/AppleWebKit\/(\d+)/)||[])[1]);


jQuery(function($){

	/* This fades in the header image
	* /
	var applyHeaderImage = function(){
		var img = $('#js-just-loading-heading-image');
		var src = img.attr('src');
		$('#header-overlay').css('background-image','url('+src+')');
		$('#header-overlay').addClass('js-loaded');
		img.remove();
	};

	if( $('#js-just-loading-heading-image').length > 0 ) {
		console.log('hi');
		if( $("#js-just-loading-heading-image")[0].complete ) {
			applyHeaderImage();
		} else {
			$("#js-just-loading-heading-image").on('load',function(){
				applyHeaderImage();
			});
		}
	}
	/**/
	
	$('.main-nav .nav-more').on('click', function(){

		if( $(this).hasClass('js-menu-active')) {

			$(this).add('body').removeClass('js-menu-active');

		} else {
			$(this).add('body').addClass('js-menu-active');
		}

		return false; 

	});

	$('.main-nav a[href="#"]').on('click',function(e){
		e.preventDefault();
	});

	$('.main-nav li.menu-item-has-children').on('click',function(e){

		if($(this).hasClass('js-on')){
			// $(this).removeClass('js-on');
		} else {
			e.preventDefault(); // allow sub nav to open
			$(this).addClass('js-on');
		}
	});

	$('.js-close-main-nav').on('click',function(e){
		e.preventDefault();
		$('.main-nav .nav-more, body').removeClass('js-menu-active');
	});

	$('.main-nav-ul li.menu-item-has-children').on('mouseenter', function(){
		$(this).addClass('js-on');
	}).on('mouseleave',function(){
		$(this).removeClass('js-on');
	});

});

/**
 * jQuery.responsiveVideo
 * Version 1.1
 * Copyright (c) 2014 c.bavota - http://bavotasan.com
 * Dual licensed under MIT and GPL.
 * Date: 01/16/2014
 *
 * 2017-10
 * Optimized slightly by Jake Paris
 *
 * 2021-08
 * Removed "embed" and "object" from the find() function to stop applying this to pdfs
 **/
jQuery(function($){
	$.fn.responsiveVideo=function(){$('head').append('<style>.responsive-video-wrapper{width:100%;position:relative;padding:0}.responsive-video-wrapper iframe,.responsive-video-wrapper object,.responsive-video-wrapper embed{position:absolute;top:0;left:0;width:100%;height:100%}</style>');$(this).find('iframe[src*="player.vimeo.com"],iframe[src*="youtube.com"],iframe[src*="youtube-nocookie.com"],iframe[src*="dailymotion.com"],iframe[src*="kickstarter.com"][src*="video.html"]').not('object object').each(function(){var $v=$(this);if($v.parents('object').length)return;if(!$v.prop('id'))$v.attr('id','rvw'+Math.floor(Math.random()*999999));$v.wrap('<div class="responsive-video-wrapper" style="padding-top:'+($v.attr('height')/$v.attr('width')*100)+'%" />').removeAttr('height').removeAttr('width')})}
	$('.wpcontent-area').responsiveVideo();
});