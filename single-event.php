<?php
get_header();

$eDates = mejp_getEventDates('F j', null, true);
$eTime = mejp_getEventTime();

if( count($eDates) ) {
	$firstDate = array_shift($eDates);

	$pastEvent = date('Ymd') > date('Ymd',strtotime($firstDate)) ? true : false;

	$firstDate = date('l, F jS',strtotime($firstDate));


	if($eTime)
		$firstDate .= ' &mdash; <i> '. $eTime . '</i>';

	$otherDates = implode(', ',$eDates);
}

?>
	<div class="content column">
	  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 

		if( $firstDate ) 
			echo '<h2 class="event-datetime">'. $firstDate .'</h2>';
						
	?> 
		

		<h1 class="page-title"><?php the_title() ?></h1>

		<?php the_post_thumbnail('large' ); ?>

		<?php if($pastEvent) echo '<div class="past-event-notification"><p>This event is over.</p></div>';
		?>

		<div class="wpcontent-area">

			
			<?php the_content() ?>

			<?php if($otherDates)
				echo '<hr><p class="post-meta future">Other upcoming dates: '.$otherDates.'</p>';
			?>

		</div><!--.wpcontent-area-->

	  <?php endwhile; endif; ?>

	</div><!--.content.column-->

<?php get_footer(); ?>
