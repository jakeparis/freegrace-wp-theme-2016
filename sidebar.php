

<div class="sidebar column">

	<?php 
	if( is_front_page() )
		dynamic_sidebar('Home Page');
	else if ( is_page('contact') || is_page('contact-us') )
		dynamic_sidebar('Contact Us');
	else
		dynamic_sidebar('main');
	?>

</div>
