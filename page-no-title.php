<?
/*
Template name: No Page Title
*/
get_header();
?>
	<div class="content column">
	  
	  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<div class="wpcontent-area">
			
			<?php the_content() ?>

		</div><!--.wpcontent-area-->

	  <?php endwhile; endif; ?>

	</div><!--.content.column-->

<?php get_footer(); ?>
